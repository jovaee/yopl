import os
from setuptools import setup


def read(file):
    return open(os.path.join(os.path.dirname(__file__), file), 'r').read().strip()


requirements = [
    "requests",
    "click",
]

setup(
    name="yopl",
    version=read('version'),
    author="Johann van Eeden",
    author_email="vaneedenj94@gmail.com",
    description="Youtube Playlist Checker",
    keywords="youtube playlist checker",
    url="",
    packages=[],
    long_description=read('readme.md'),
    install_requires=requirements,
    classifiers=[],
)
