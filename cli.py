import click

from cli_groups.run import playlist


@click.group()
def cli():
	pass


# Add all the CLI groups
cli.add_command(playlist)

if __name__ == '__main__':
	cli()
