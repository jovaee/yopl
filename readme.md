Yopl
-------
A CLI that allows the checking of what videos are not longer regarded as "active" in a Youtube playlist. This allows the user to find the videos again on other channels and not have missing videos in the playlist. This is mostly indented for music playlists where you don't want songs to disappear from it.

#### Command
Print out al the videos in the playlist

    $ cli playlist -p <playlist_id> ls

Save the current videos in the playlist to a file

    $ cli playlist -p <playlist_id> save

Compare the saved file's videos list with the current videos in the playlist

    $ cli playlist -p <playlist_id> compare
