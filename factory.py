import json

from clients.youtube import YouTubeClient


class Factory:
    """

    """

    @staticmethod
    def create_youtube_client():
        """
        Create an instance of the Youtube client
        """
        with open('secret.json', 'r') as secret:
            api_key = json.load(secret)['api_key']
            return YouTubeClient(api_key)
