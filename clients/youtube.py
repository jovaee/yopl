import requests

from typing import List, Dict


YOUTUBE_API = 'https://www.googleapis.com/youtube/v3/playlistItems'


class YouTubeClient:
    """

    """

    def __init__(self, api_key: str):
        self.api_key = api_key

    @staticmethod
    def transform_playlist_data(data) -> List[Dict]:
        """
        Transform the Youtube API response data into a more desirable format
        :param data:
        """
        return [{
            'id': item['id'],
            'name': item['snippet']['title'],
            'url': item['snippet']['resourceId']['videoId'],

        } for item in data.get('items', [])]

    @staticmethod
    def filter_removed_videos(data):
        """
        Remove videos that are not longer "active"
        :param data:
        """
        return [item for item in data if item['name'] not in ['Private video', 'Deleted video']]

    def get_playlist_page_videos(self, playlist_id: str, page_token: str = None):
        """
        Get the videos of a single page of the playlist
        :param playlist_id:
        :param page_token:
        """
        params = {
            'part': 'snippet',
            'playlistId': playlist_id,
            'key': self.api_key,
            'maxResults': 50,
        }

        if page_token:
            params['pageToken'] = page_token

        results = requests.get(url=YOUTUBE_API, params=params)
        return results.json()

    def get_playlist_videos(self, playlist_id: str, dirty: bool=True) -> List[Dict]:
        """
        Get all the videos of a playlist
        :param playlist_id:
        """
        result = self.get_playlist_page_videos(playlist_id)

        return_value = self.transform_playlist_data(result)
        while 'nextPageToken' in result:
            result = self.get_playlist_page_videos(playlist_id, result['nextPageToken'])
            return_value.extend(self.transform_playlist_data(result))

        if not dirty:
            return_value = self.filter_removed_videos(return_value)
        return return_value
