import click
import json
import os

from factory import Factory


@click.group()
@click.option('--playlist_id', '-p', required=True, help='The ID of the playlist to check')
@click.pass_context
def playlist(ctx, playlist_id):
	ctx.ensure_object(dict)
	ctx.obj['playlist_id'] = playlist_id

	client = Factory.create_youtube_client()
	ctx.obj['client'] = client

	
@playlist.command()
@click.option('--dirty', '-d', is_flag=True, help='Don\'t remove private and deleted videos')
@click.pass_context
def ls(ctx, dirty):
	print('Retrieving playlist videos')
	videos = ctx.obj['client'].get_playlist_videos(ctx.obj['playlist_id'], dirty)

	print(f'You have a total of {len(videos)} active videos in the playlist')
	for index, video in enumerate(videos):
		print(f"[{index:>4}] {video['name']} https://www.youtube.com/watch?v={video['url']}")


@playlist.command()
@click.option('--dirty', '-d', is_flag=True, help='Don\'t remove private and deleted videos')
@click.option('--output', '-o', default='output.json', help='Name of the file to save results to')
@click.pass_context
def save(ctx, output, dirty):
	print('Retrieving playlist videos')
	videos = ctx.obj['client'].get_playlist_videos(ctx.obj['playlist_id'], dirty)

	with open(output, 'w') as file:
		json.dump(videos, file, indent=2)

	print(f'Videos saved to file "{output}"')
	print(f'Saved {len(videos)} videos')


@playlist.command()
@click.option('--source', '-s', default='output.json', help='Name of the file to save results to')
@click.option('--output', '-o', default='compared.json', help='Name of the file to save results to')
@click.pass_context
def compare(ctx, source, output):
	# Make sure the input file existts
	if not os.path.exists(source):
		print('The input file "%s" specified does not exist' % source)
		return

	print('Retrieving latest playlist videos')
	videos = ctx.obj['client'].get_playlist_videos(ctx.obj['playlist_id'], False)

	with open(source, 'r') as input_file, open(output, 'w') as output_file:
		source_videos = json.load(input_file)

		# Check what videos were in the source file but are not present in the requested list
		results = []
		for video in source_videos:
			matches = [v for v in videos if v['id'] == video['id']]
			if not matches:
				results.append(video)

		json.dump(results, output_file, indent=2)
		print(f'Results saved to file "{output}"')

		print(f'Missing {len(results)} videos')
		for index, video in enumerate(results):
			print(f"[{index:>4}] {video['name']} https://www.youtube.com/watch?v={video['url']}")
